import { FaceBookComponent } from './facebook/facebook.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TwitterComponent } from './twitter/twitter.component';

const routes: Routes = [
  { path: 'twitter' , component: TwitterComponent },
  { path: 'facebook' , component: FaceBookComponent },
  { path: '**', redirectTo: 'twitter' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
