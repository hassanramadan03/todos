import { Injectable } from '@angular/core';
import {  RequestOptions } from "@angular/http";
import { HttpHeaders } from "@angular/common/http";


@Injectable()
export class ApiUrlService {
    private baseUrl = 'https://zappyelements.herokuapp.com/';
    public socketUrl='https://zappyelements.herokuapp.com'
     
    private header: any = {};
    constructor() {
        this.header = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            })
        }
    }
    getHeaders() {
        return new RequestOptions(this.header);
    }
    getApiUrl() {
        return {
            getAllTwts: this.baseUrl + `twitter/allTwts`,
            getAllPosts: this.baseUrl + `fb/allPosts`,
            addTweet:this.baseUrl+`twitter/add`
           }
    }
}