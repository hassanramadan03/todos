import { ApiUrlService } from "./api_url.service";
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FBService {
    constructor(private http: HttpClient, private apiUrlService: ApiUrlService) {
    }
    getAllPosts() {
        let header:any=this.apiUrlService.getHeaders();
        return this.http.get(this.apiUrlService.getApiUrl().getAllPosts,header ).map((res:any)=>res.allPosts)
    }
    
}
