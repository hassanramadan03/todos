import { SharedService } from './shared.service';
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import * as io from 'socket.io-client'
import { Observable } from 'rxjs/Observable'
import * as Rx from 'rxjs/Rx'
import { ApiUrlService } from "./api_url.service";
@Injectable()
export class WebSocketService {
    private socket;
    constructor(private apiUrlService: ApiUrlService,private SharedService:SharedService) {
    }
    connectGoHitted(): Rx.Subject<MessageEvent> {
        this.socket = io(`${this.apiUrlService.socketUrl}`);
        let observable = new Observable(Observer => {
            this.socket.on('goHitted', ({go}) => {
                this.SharedService.openSnackBar(go,'success')
                Observer.next({go})
            })
            return () => { this.socket.disconnect() }
        })
        let observer = {
            next: (data: Object) => {
            }
        }
        return Rx.Subject.create(observer, observable)
    }

}
 
