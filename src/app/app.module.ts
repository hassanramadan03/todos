import { FeedService } from './_services/feed.service';
import { TwitterService } from './_services/twitter.service';
import { FBService } from './_services/fb.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { WebSocketService } from './_services/webSocket.service'
import { ApiUrlService } from './_services/api_url.service';
import { HttpClientModule } from '@angular/common/http';
import {  SharedService } from './_services/shared.service';
import { HttpModule } from '@angular/http';
import { MatComponentsModule } from './angularComponents';
import { TwitterComponent, DialogOverviewExampleDialog } from './twitter/twitter.component';
import { FaceBookComponent } from './facebook/facebook.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TwitterComponent,
    FaceBookComponent,
    DialogOverviewExampleDialog,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    MatComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    // SellerMangementComponent,

  ],
  providers: [
    WebSocketService,
    ApiUrlService,
    FeedService,
    TwitterService,
    FBService,
    HttpClientModule,
    SharedService
  ],
  entryComponents: [DialogOverviewExampleDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
