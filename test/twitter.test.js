process.env.NODE_ENV = 'test'
const chai = require('chai')
const should = chai.should()
const request = require('supertest')
const app = require('../app').app;
const asserts = require('chai').assert;
const {
    saveTWsInDB,
    handleTWPosts
}=require('./../buisness_modules/twitter/service');

describe('__User API Integration Tests__', () => {

    describe('- POST /Twitter/add', () => {
        //you have to change tweet every time
        const tweet = {
            tweet: `this tweet is created in ${new Date()}`,
        }
        it('should add a new & not Duplicated Tweet on my wall ', done => {
            request(app)
                .post('/Twitter/add')
                .send(tweet)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    should.not.exist(err)
                    res.body.should.include.keys('ok', 'message')
                    should.exist(res.body.ok)
                    res.body.ok.should.equal(true)
                    should.exist(res.body.message)
                    done()
                })
        })

    })
    describe('- GET /Twitter/allTwts', () => {
        it('should return all Tweets stored in the DB', done => {
            request(app)
                .get('/twitter/allTwts')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    should.not.exist(err)
                    res.body.should.include.keys('allTwts', 'ok')
                    done()
                })
        })
    })

})

describe('save Tweets in DB',()=>{
    it('should store tweets in DB that are not stored before and return true',async()=>{
        const tweet={id:'123','created_at':new Date(),text:'example text for test'};
        const TWTs=[tweet]
        let result =await saveTWsInDB(TWTs)
        asserts.equal(result,true);
    })
})
 
describe('handleTWPosts ',async()=>{
    it('should get all Tweets from my wall and store them in DB and Return true ',async()=>{
        let result =await handleTWPosts()
        asserts.equal(result,true);
    })
})
