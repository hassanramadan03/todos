const cors = require('cors'),
    path = require('path'),
    bodyParser = require('body-parser'),
    express = require('express'),
    app = express(),
    http = require('http').Server(app) ,
    mongoose=require('mongoose'),
    router=require('./buisness_modules/router') ;
    
    require('dotenv').config({ silent: true });

const dbHost =process.env.mongooseConfig
exports.io= require('socket.io')(http);

   
const { createEventAdapter } = require('@slack/events-api');
const slackEvents = createEventAdapter(process.env.SLACK_SIGNING_SECRET);
const {handeSlackEvents}=require('./buisness_modules/slack/controller');
    
    app.use('/slack/events', slackEvents.expressMiddleware());
    slackEvents.on('message',handeSlackEvents);
    slackEvents.on('error', console.error);
    slackEvents.start();

app.set('port', (process.env.PORT || '3000'));
app.use(cors({ origin: '*' }));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.json({  type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
 

app.use('/twitter',router.twitter)
app.use('/fb',router.fb)
 
// Force HTTPS on Heroku

if (app.get('env') === 'mediaion') {
    app.use(function (req, res, next) {
        var protocol = req.get('x-forwarded-proto');
        protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
    });
}
 
  
app.use(express.static(path.join(__dirname, "js")));
app.use(express.static(path.join(__dirname, 'dist/navbar')));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/dist/navbar/index.html'));
})


mongoose.connect(dbHost, {useNewUrlParser: true}).then(async res => {
    
    http.listen(app.get('port'), function () {
        console.log('Listening to port:  ' + app.get('port'));
    });
    
})
 

exports.app = app;

