require('dotenv').config()
const Twit = require('twit')
const twitter_config= new Twit({
    consumer_key: process.env.consumer_key,
    consumer_secret: process.env.consumer_secret,
    access_token: process.env.access_token,
    access_token_secret: process.env.access_token_secret,
    timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
    strictSSL: true, // optional - requires SSL certificates to be valid.
})
const slackChannelInfo=(token,id)=> `https://slack.com/api/channels.info?token=${token}&channel=${id}&include_locale=true`;
module.exports = {
    twitter_config,
    slackChannelInfo

};