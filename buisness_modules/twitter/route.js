const express = require('express');
const router = express.Router();
const {getAllTwts,addTweet} = require('./controller');

router.get('/allTwts', getAllTwts );
router.post('/add', addTweet );

module.exports = router;